import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'reaction_controller.g.dart';

@Injectable()
class ReactionController = _ReactionControllerBase with _$ReactionController;

abstract class _ReactionControllerBase with Store {
  @observable
  int valueA = 0;

  @observable
  int valueB = 0;

  @computed
  int get amount => valueA + valueB;

  @action
  void incrementA() {
    valueA++;
  }

  @action
  void incrementB() {
    valueB++;
  }
}
