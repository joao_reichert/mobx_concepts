import 'reaction_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'reaction_page.dart';

class ReactionModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $ReactionController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => ReactionPage()),
      ];

  static Inject get to => Inject<ReactionModule>.of();
}
