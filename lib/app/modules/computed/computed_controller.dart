import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'computed_controller.g.dart';

@Injectable()
class ComputedController = _ComputedControllerBase with _$ComputedController;

abstract class _ComputedControllerBase with Store {
  @observable
  int valueA = 0;

  @observable
  int valueB = 0;

  @computed
  int get amount => valueA + valueB;

  @action
  void incrementA() {
    valueA++;
  }

  @action
  void incrementB() {
    valueB++;
  }
}
