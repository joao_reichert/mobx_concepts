import 'computed_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'computed_page.dart';

class ComputedModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $ComputedController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => ComputedPage()),
      ];

  static Inject get to => Inject<ComputedModule>.of();
}
