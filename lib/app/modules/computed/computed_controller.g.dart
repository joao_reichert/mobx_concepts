// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'computed_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $ComputedController = BindInject(
  (i) => ComputedController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ComputedController on _ComputedControllerBase, Store {
  Computed<int> _$amountComputed;

  @override
  int get amount => (_$amountComputed ??= Computed<int>(() => super.amount,
          name: '_ComputedControllerBase.amount'))
      .value;

  final _$valueAAtom = Atom(name: '_ComputedControllerBase.valueA');

  @override
  int get valueA {
    _$valueAAtom.reportRead();
    return super.valueA;
  }

  @override
  set valueA(int value) {
    _$valueAAtom.reportWrite(value, super.valueA, () {
      super.valueA = value;
    });
  }

  final _$valueBAtom = Atom(name: '_ComputedControllerBase.valueB');

  @override
  int get valueB {
    _$valueBAtom.reportRead();
    return super.valueB;
  }

  @override
  set valueB(int value) {
    _$valueBAtom.reportWrite(value, super.valueB, () {
      super.valueB = value;
    });
  }

  final _$_ComputedControllerBaseActionController =
      ActionController(name: '_ComputedControllerBase');

  @override
  void incrementA() {
    final _$actionInfo = _$_ComputedControllerBaseActionController.startAction(
        name: '_ComputedControllerBase.incrementA');
    try {
      return super.incrementA();
    } finally {
      _$_ComputedControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void incrementB() {
    final _$actionInfo = _$_ComputedControllerBaseActionController.startAction(
        name: '_ComputedControllerBase.incrementB');
    try {
      return super.incrementB();
    } finally {
      _$_ComputedControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
valueA: ${valueA},
valueB: ${valueB},
amount: ${amount}
    ''';
  }
}
