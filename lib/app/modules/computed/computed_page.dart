import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'computed_controller.dart';

class ComputedPage extends StatefulWidget {
  final String title;
  const ComputedPage({Key key, this.title = "Computed"}) : super(key: key);

  @override
  _ComputedPageState createState() => _ComputedPageState();
}

class _ComputedPageState
    extends ModularState<ComputedPage, ComputedController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: [
                const Text(
                  'Você apertou os botões:',
                ),
                Observer(
                  builder: (_) => Text(
                    '${controller.amount} vezes',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                const Text(
                  'Você apertou o botão A:',
                ),
                Observer(
                  builder: (_) => Text(
                    '${controller.valueA} vezes',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                const Text(
                  'Você apertou o botão B:',
                ),
                Observer(
                  builder: (_) => Text(
                    '${controller.valueB} vezes',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton(
              heroTag: '1',
              onPressed: controller.incrementA,
              child: Text('A'),
            ),
            FloatingActionButton(
              heroTag: '2',
              onPressed: controller.incrementB,
              child: Text('B'),
            ),
          ],
        ),
      ),
    );
  }
}
