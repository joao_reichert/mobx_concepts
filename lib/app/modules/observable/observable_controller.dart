import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'observable_controller.g.dart';

@Injectable()
class ObservableController = _ObservableControllerBase
    with _$ObservableController;

abstract class _ObservableControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
