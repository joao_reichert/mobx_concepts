import 'observable_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'observable_page.dart';

class ObservableModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $ObservableController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => ObservablePage()),
      ];

  static Inject get to => Inject<ObservableModule>.of();
}
