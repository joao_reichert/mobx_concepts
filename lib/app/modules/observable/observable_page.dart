import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'observable_controller.dart';

class ObservablePage extends StatefulWidget {
  final String title;
  const ObservablePage({Key key, this.title = "Observable"}) : super(key: key);

  @override
  _ObservablePageState createState() => _ObservablePageState();
}

class _ObservablePageState
    extends ModularState<ObservablePage, ObservableController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Você apertou o botão tudo isso aê:',
            ),
            Observer(
              builder: (_) => Text(
                '${controller.value}',
                style: const TextStyle(fontSize: 20),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: controller.increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
