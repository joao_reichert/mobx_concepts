import 'package:core_concepts/app/modules/autorun/autorun_module.dart';
import 'package:core_concepts/app/modules/computed/computed_module.dart';
import 'package:core_concepts/app/modules/observable/observable_module.dart';
import 'package:core_concepts/app/modules/reaction/reaction_module.dart';
import 'package:core_concepts/app/modules/when/when_module.dart';

import 'home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_page.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $HomeController,
      ];

  @override
  final List<ModularRouter> routers = [
    ModularRouter(Modular.initialRoute, child: (_, args) => HomePage()),
    ModularRouter('/observable', module: ObservableModule()),
    ModularRouter('/computed', module: ComputedModule()),
    ModularRouter('/autorun', module: AutorunModule()),
    ModularRouter('/reaction', module: ReactionModule()),
    ModularRouter('/when', module: WhenModule()),
  ];

  static Inject get to => Inject<HomeModule>.of();
}
