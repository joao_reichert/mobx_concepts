import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  final List<DemoOption> options = [
    DemoOption(title: 'Observable', path: '/observable'),
    DemoOption(title: 'Computed Observables', path: '/computed'),
    DemoOption(title: 'Autorun', path: '/autorun'),
    DemoOption(title: 'Reaction', path: '/reaction'),
    DemoOption(title: 'When', path: '/when'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mobx Demo'),
      ),
      body: ListView.separated(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(options[index].title),
            onTap: () => Modular.to.pushNamed(options[index].path),
            trailing: Icon(Icons.chevron_right),
          );
        },
        separatorBuilder: (context, index) => Divider(),
        itemCount: options.length,
      ),
    );
  }
}

class DemoOption {
  final String title;
  final String path;

  DemoOption({@required this.title, @required this.path});
}
