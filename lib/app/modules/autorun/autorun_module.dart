import 'autorun_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'autorun_page.dart';

class AutorunModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $AutorunController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => AutorunPage()),
      ];

  static Inject get to => Inject<AutorunModule>.of();
}
