import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'autorun_controller.g.dart';

@Injectable()
class AutorunController = _AutorunControllerBase with _$AutorunController;

abstract class _AutorunControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
