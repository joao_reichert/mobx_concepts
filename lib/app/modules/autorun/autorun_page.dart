import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'autorun_controller.dart';

class AutorunPage extends StatefulWidget {
  final String title;
  const AutorunPage({Key key, this.title = "Autorun"}) : super(key: key);

  @override
  _AutorunPageState createState() => _AutorunPageState();
}

class _AutorunPageState extends ModularState<AutorunPage, AutorunController> {
  //use 'controller' variable to access controller

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  ReactionDisposer _disposer;

  @override
  void initState() {
    super.initState();

    _disposer = autorun((_) {
      print(controller.value);
      this.showSnackbar();
    });
  }

  @override
  void dispose() {
    _disposer.call();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('Click'),
              onPressed: controller.increment,
            ),
            Observer(
              builder: (context) {
                return Text('${controller.value}');
              },
            )
          ],
        ),
      ),
    );
  }

  void showSnackbar() {
    final snackBar = SnackBar(
      content: Text('Yay! A SnackBar! ${controller.value}'),
      duration: Duration(milliseconds: 500),
      action: SnackBarAction(
        label: 'Undo',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
