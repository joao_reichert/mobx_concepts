import 'when_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'when_page.dart';

class WhenModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $WhenController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => WhenPage()),
      ];

  static Inject get to => Inject<WhenModule>.of();
}
