import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'when_controller.dart';

class WhenPage extends StatefulWidget {
  final String title;
  const WhenPage({Key key, this.title = "When"}) : super(key: key);

  @override
  _WhenPageState createState() => _WhenPageState();
}

class _WhenPageState extends ModularState<WhenPage, WhenController> {
  //use 'controller' variable to access controller

  ReactionDisposer _disposer;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();

    _disposer = when((_) => controller.valueA == 5, () {
      this.showSnackbar();
    });
  }

  @override
  void dispose() {
    _disposer();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: [
                const Text(
                  'Você apertou os botões:',
                ),
                Observer(
                  builder: (_) => Text(
                    '${controller.amount} vezes',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                const Text(
                  'Você apertou o botão A:',
                ),
                Observer(
                  builder: (_) => Text(
                    '${controller.valueA} vezes',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                const Text(
                  'Você apertou o botão B:',
                ),
                Observer(
                  builder: (_) => Text(
                    '${controller.valueB} vezes',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton(
              heroTag: '1',
              onPressed: controller.incrementA,
              child: Text('A'),
            ),
            FloatingActionButton(
              heroTag: '2',
              onPressed: controller.incrementB,
              child: Text('B'),
            ),
          ],
        ),
      ),
    );
  }

  void showSnackbar() {
    final snackBar = SnackBar(
      content: Text('Yay! A SnackBar! ${controller.valueA}'),
      duration: Duration(milliseconds: 500),
      action: SnackBarAction(
        label: 'Undo',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
