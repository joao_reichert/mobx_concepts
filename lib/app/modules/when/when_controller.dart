import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'when_controller.g.dart';

@Injectable()
class WhenController = _WhenControllerBase with _$WhenController;

abstract class _WhenControllerBase with Store {
  @observable
  int valueA = 0;

  @observable
  int valueB = 0;

  @computed
  int get amount => valueA + valueB;

  @action
  void incrementA() {
    valueA++;
  }

  @action
  void incrementB() {
    valueB++;
  }
}
